;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defpackage #:cl-gamelib-asd
  (:use :cl :asdf))

(in-package :cl-gamelib-asd)

(defsystem cl-gamelib
  :name "cl-gamelib"
  :version "0.0.0"
  :maintainer "A. Masyakin"
  :author "Anton Masyakin"
  :licence "Not yet"
  :description "CL game programming halpers"
  :long-description "--"
  :components ((:module cl-gamelib-helpers
                        :pathname "helpers"
                        :components ((:file package)
                                     (:file helpers)))
               (:module cl-gamelib-vectors
                        :pathname "vector"
                        :depends-on ("cl-gamelib-helpers")
                        :components ((:file package)
                                     (:file vector)))
               (:module cl-gamelib-matrix
                        :pathname "matrix"
                        :depends-on ("cl-gamelib-helpers"
                                     "cl-gamelib-vectors")
                        :components ((:file package)
                                     (:file matrix)))
               (:module cl-gamelib-quaternions
                        :pathname "quaternion"
                        :depends-on ("cl-gamelib-helpers"
                                     "cl-gamelib-vectors")
                        :components ((:file package)
                                     (:file quaternion)))))
                                     
