(in-package :cl-gamelib-helpers)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require 'cl-ppcre)
  (require 'alexandria))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter *speed-opt* 3)
  (defparameter *safe-opt*  1)
  (defparameter *debug-opt* 1)

  (defmacro to-string (arg)
    "Turn arg ito string"
    `(etypecase ,arg
       (string ,arg)
       (character (string ,arg))
       (symbol (symbol-name ,arg))
       (number (write-to-string ,arg))
       ;;(t (break))
    ))

  (defun buildkw (&rest rest)
    "build symbol from parts(strings or symbols)"
    (let ((alist (remove nil rest)))
      (intern (reduce (lambda (&optional (a "") (b ""))
                        (concatenate 'string
                                     a
                                     (string-upcase (to-string b))))
                      alist :initial-value "" ) "KEYWORD")))
  
  (defun buildsym (&rest rest)
    "build symbol from parts(strings or symbols)"
    (let ((alist (remove nil rest)))
      (intern (reduce (lambda (&optional (a "") (b ""))
                        (concatenate 'string
                                     a
                                     (string-upcase (to-string b))))
                      alist :initial-value "" ))))

  (defun regex-whole-matchp (str reg)
    "T if str matches regular expression reg completely"
    (let ((len (length str)))
      (multiple-value-bind (s e)
          (cl-ppcre:scan reg str)
        (if (and s
                 (= s 0)
                 (= len e))
            t
            nil))))
  
  (defun regex-part-matchp (str reg)
    "T if str contains regular expression reg"
    (finish-output)
    (if (cl-ppcre:scan reg str) t nil))


  (defun part-subst-regex (sym place replace)
    "Substitute matching(with place) part of a symbol sym with replace"
    (cl-ppcre:regex-replace-all
     (to-string place)
     (to-string sym)
     (to-string replace)))
  
  (defun f-subst-regex (sym place replace)
    "Substitute sym with replace partioally or completely epending on match with place"
    (if (stringp sym)
        sym
        (let ((sym-s (string-upcase (to-string sym)))
              (place-s (to-string place)))
          (cond
            ((regex-whole-matchp sym-s place-s) replace)
            ((regex-part-matchp sym-s place-s)
             (let ((res (part-subst-regex sym place replace)))
               (etypecase sym
                 (string res)
                 (symbol (intern (string-upcase res))))))
            (t sym)))))
  
  (defun f-replace-with (form pairs)
    "look through form and if any symbol matches with any sybstitute pair - replace"
    (labels ((replace-places (place)
               (let ((nplace place))
                 (loop for p in pairs
                    do(if (listp nplace)
                          (walk-list nplace)
                          (if (first p)
                              (setf nplace (f-subst-regex nplace (first p) (second p))))))
                 nplace))
             (walk-list (l)
               (loop for i in l
                  collecting (if (listp i)
                                 (walk-list i)
                                 (replace-places i)))))
      (if (listp form)
          (walk-list form)
          (replace-places form))))
  
  (defun f-combine (as bs with &key (syma '{A}) (symb '{B}))
    "For each of as combine it with each in bs using expression in with"
    (loop for a in as
       collect (loop for b in bs
                  collect (f-replace-with with `((,syma ,a) (,symb ,b))))))
  
  (defun f-apply (with list) ;; with = (+ !@ )
    "Put with as first element of lists"
    (if with
        (if (listp with)
            (f-replace-with with (list (list '{x} list)))
            (if (listp list)
                (cons with list)
                (list with list)))
        list)) ;; eval-when end
  
  (defun f-apply-each (with lists) ;; with = (+ !@ )
    "Put sym with as first element of each list in list"
;;    (if with
    (loop for i in lists
       collect (f-apply with i)))

  (defun f-rising-lists (list &optional (start 1))
    (let ((ll (length list)))
      (loop for i from start to ll
         collect (subseq list 0 i))))

  (defun float= (a b)
    (declare (type single-float a b))
    (< (abs (- a b)) 0.00001))  
  );;EVAL-WHEN
  ;;      lists))

(defmacro defrecop (name binop &key argtype declarations)
  "Define recursive operator from biary"
  (alexandria:with-gensyms(arg1 arg2 arg-rest result)
    `(defun ,name (,arg1 ,arg2 &rest ,arg-rest)
       ,@(loop for d in declarations
               collect`(declare ,d))
       ,(if argtype
            `(declare (type ,argtype ,arg1 ,arg2)))
       (let ((,result (,binop ,arg1 ,arg2)))
         (if ,arg-rest
             (,name ,result (car ,arg-rest) (cdr ,arg-rest))
             ,result)))))
       
;;(defrecop a %a :argtype 'vec :declarations ((inline %a)))

(defmacro defopt (name args &body body)
  "Define optimization declared function"
  `(defun ,name ,args
     (declare (optimize (speed ,*speed-opt*) (safety ,*safe-opt*) (debug ,*debug-opt*)))
     ,@body))

(defmacro m-replace-with (form &rest pairs)
  (f-replace-with form pairs))

(defmacro m-apply-each (lists &key with)
  (f-apply-each with lists))

(defmacro m-apply (lists &key with)
  (f-apply with lists))  

(defmacro m-combine-list (lists &key with (each nil) (all nil) )
  (f-apply all
            (f-apply-each each
                  (f-combine (second lists) (first lists) with))))

(defmacro m-combine (objs &key by (with nil)
                            (each nil) (group nil)
                            (all nil) (wrap nil)
                            (sanitize t))
  "Combine as with bs applying forms"
  (labels ((applay-mod (mod replace-token replace)
             (if (and mod (listp mod))
                 (f-replace-with mod (list (list replace-token replace)))
                 (f-apply mod replace)))
           (unwrap-l (list)
             (if (and (listp list) (= (length list) 1))
                 (car list)
                 list))
           (sanitize (list)
             (let ((sanlist (unwrap-l list)))
               (if (listp sanlist)
                   (loop for i in sanlist
                         collect (sanitize i))
                   sanlist))))
    
    (let* ((combined (f-combine objs by with :syma '{o} :symb '{f}))
           (eached (loop for obj in combined
                         collect (loop for field in obj
                                       collect (applay-mod each '{x} field))))
           (grouped (apply #'mapcar #'list eached))
           (group-applied (loop for g in grouped
                                collect (applay-mod group '{x} g)))
           (all-applied (applay-mod all '{x} group-applied))
           (wrapped (applay-mod wrap '{x} all-applied)))

      (if sanitize
          (sanitize wrapped)
          wrapped))))

;;(m-rising-lists (q w e r) 2)


;;(m-combine (nil) :by (l p r) :with (vec-{f} ))
;;(m-combine (a b) :by (l p r) :with (vec-{f} {o}) :each (* 2 {x}))
;;(m-combine (a b) :by (l p r) :with (vec-{f} {o}) :each (* 2 {x}) :group + :all (apply #'/ {x}) :wrap (- {x} 1))

;;(m-combine (a) :by (l p r) :with (vec-{f} {o}) :each (* 2 {x}) :all (apply #'/ {x}) :wrap (- {x} 1))

;;math and such
(declaim (inline deg-to-rad))
(defun deg-to-rad (deg)
  (* deg (/ pi 180)))

(declaim (inline rad-to-deg))
(defun rad-to-deg (rad)
  (* rad (/ 180.0 pi)))

(declaim (inline clamp))
(defun clamp (n &optional (min -1.0) (max 1.0))
  (alexandria:clamp n min max))
  
(declaim (inline ))
(defun clamp (n &optional (min -1.0) (max 1.0))
  (alexandria:clamp n min max))


(defmacro defsequenced-constants (seq-name &rest names)
  (let ((val 0))
  `(progn ,@(loop for name in names
               collect`(defconstant ,(buildsym '+ seq-name '- name '+) ,val)
                 do(setf val (1+ val))))))

  
