(defpackage cl-gamelib-helpers
  (:nicknames :cl-gal-h :gal-h :gh)
  (:use common-lisp)
  (:export :buildsym
           :buildkw
           :defopt
           :defrecop
           :f-replace-with
           :f-apply
           :f-combine
           :m-combine
           :float=
           :rad-to-deg
           :deg-to-rad
           :clamp
           :defsequenced-constants
           )) 
