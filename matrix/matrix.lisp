(in-package #:cl-gamelib-matrix)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require 'alexandria))


(defstruct (m2 (:conc-name m2-)
               (:print-function print-m2)
                 (:constructor)
                 (:constructor %m2 (x y)))
  (x (v2) :type gv:v2)
  (y (v2) :type gv:v2))

(defstruct (m3 (:conc-name m3-)
               (:print-function print-m3)
                 (:constructor)
                 (:constructor %m3 (x y z)))
  (x (v3) :type gv:v3)
  (y (v3) :type gv:v3)
  (z (v3) :type gv:v3))

(defstruct (m4 (:conc-name m4-)
               (:print-function print-m4)
                 (:constructor)
                 (:constructor %m4 (x y z w)))
  (x (v4) :type gv:v4)
  (y (v4) :type gv:v4)
  (z (v4) :type gv:v4)
  (w (v4) :type gv:v4))


(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun mat-index (idx)
    "1 2 3 4 -> x y z w / x y z w -> x y z w"
    (case idx
      ((0 x) 'x)
      ((1 y) 'y)
      ((2 z) 'z)
      ((3 w) 'w)))
  (defun mat-index-n (idx)
    "1 2 3 4 -> x y z w / x y z w -> x y z w"
    (case idx
      ((0 x) 0)
      ((1 y) 1)
      ((2 z) 2)
      ((3 w) 3))))

(defun m2ref (mat row col)
  (nth row (v2-list (nth col (m2-list mat)))))

(defun m3ref (mat row col)
  (nth row (v3-list (nth col (m3-list mat)))))

(defun m4ref (mat row col)
  (nth row (v4-list (nth col (m4-list mat)))))

(defmacro m2ref-m (mat row col)
  (let ((r (mat-index row))
        (c (mat-index col)))
    (f-replace-with `(v2-{R} (m2-{C} ,mat)) (list (list '{R} r)
                                                  (list '{C} c)))))

(defmacro m3ref-m (mat row col)
  (let ((r (mat-index row))
        (c (mat-index col)))
    (f-replace-with `(v3-{R} (m3-{C} ,mat)) (list (list '{R} r)
                                                  (list '{C} c)))))

(defmacro m4ref-m (mat row col)
  (let ((r (mat-index row))
        (c (mat-index col)))
    (f-replace-with `(v4-{R} (m4-{C} ,mat)) (list (list '{R} r)
                                                  (list '{C} c)))))

;;(m2ref-m mum 1 2)
;;(m3ref-m  mum x z)
;;(m4ref-m mum 3 4)
        
;;to list
(defun m2-list (m)
  (m-combine (m) :by (x y)
             :with (m2-{f} {O})
             :all list))

(defun m3-list (m)
  (m-combine (m) :by (x y z)
             :with (m3-{f} {O})
             :all list))

(defun m4-list (m)
  (m-combine (m) :by (x y z w)
             :with (m4-{f} {O})
             :all list))

(defun m2arr (m)
  (let ((res (make-array '(4) :element-type 'single-float)))
    (setf (aref res 0) (m2ref-m m 0 0))
    (setf (aref res 1) (m2ref-m m 1 0))
    (setf (aref res 2) (m2ref-m m 0 1))
    (setf (aref res 3) (m2ref-m m 1 1))
    res))

(defun m3arr (m)
  (let ((res (make-array '(9) :element-type 'single-float)))
    (setf (aref res 0) (m3ref-m m 0 0))
    (setf (aref res 1) (m3ref-m m 1 0))
    (setf (aref res 2) (m3ref-m m 2 0))

    (setf (aref res 3) (m3ref-m m 0 1))
    (setf (aref res 4) (m3ref-m m 1 1))
    (setf (aref res 5) (m3ref-m m 2 1))

    (setf (aref res 6) (m3ref-m m 0 2))
    (setf (aref res 7) (m3ref-m m 1 2))
    (setf (aref res 8) (m3ref-m m 2 2))
    res))

(defun m4arr (m)
  (let ((res (make-array '(16) :element-type 'single-float)))
    (setf (aref res 0) (m4ref-m m 0 0))
    (setf (aref res 1) (m4ref-m m 1 0))
    (setf (aref res 2) (m4ref-m m 2 0))
    (setf (aref res 3) (m4ref-m m 3 0))

    (setf (aref res 4) (m4ref-m m 0 1))
    (setf (aref res 5) (m4ref-m m 1 1))
    (setf (aref res 6) (m4ref-m m 2 1))
    (setf (aref res 7) (m4ref-m m 3 1))

    (setf (aref res 8) (m4ref-m m 0 2))
    (setf (aref res 9) (m4ref-m m 1 2))
    (setf (aref res 10) (m4ref-m m 2 2))
    (setf (aref res 11) (m4ref-m m 3 2))

    (setf (aref res 12) (m4ref-m m 0 3))
    (setf (aref res 13) (m4ref-m m 1 3))
    (setf (aref res 14) (m4ref-m m 2 3))
    (setf (aref res 15) (m4ref-m m 3 3))
    res))


(defun m2 (&optional (xx 0.0) (xy 0.0) (yx 0.0) (yy 0.0))
  ;;  (declare (inline v2))
  (m-combine (x y) :by (x y)
             :with ({F}{O})
             :group v2
             :all %m2))          
  
(defun m3 (&optional
             (xx 0.0) (xy 0.0) (xz 0.0)
             (yx 0.0) (yy 0.0) (yz 0.0)
             (zx 0.0) (zy 0.0) (zz 0.0))
;;  (declare (inline v2))
  (m-combine (x y z) :by (x y z)
             :with ({F}{O})
             :group v3
             :all %m3))

(defun m4 (&optional
             (xx 0.0) (xy 0.0) (xz 0.0) (xw 0.0)
             (yx 0.0) (yy 0.0) (yz 0.0) (yw 0.0)
             (zx 0.0) (zy 0.0) (zz 0.0) (zw 0.0)
             (wx 0.0) (wy 0.0) (wz 0.0) (ww 0.0))
;;  (declare (inline v2))
  (m-combine (x y z w) :by (x y z w)
             :with ({F}{O})
             :group v4
             :all %m4))

(defun m2-to-m3 (m &optional (zz 0.0))
  (%m3 (%v3 (m2ref-m m x x) (m2ref-m m y x) 0.0)
       (%v3 (m2ref-m m x y) (m2ref-m m y y) 0.0)
       (%v3 0.0 0.0 zz)))

(defun m3-to-m4 (m &optional (ww 0.0))
  (%m4 (%v4 (m3ref-m m x x) (m3ref-m m y x) (m3ref-m m z x) 0.0)
       (%v4 (m3ref-m m x y) (m3ref-m m y y) (m3ref-m m z y) 0.0)
       (%v4 (m3ref-m m x z) (m3ref-m m y z) (m3ref-m m z z) 0.0)
       (%v4 0.0 0.0 0.0 ww)))

(defun m2-to-m4 (m &optional (ww 0.0))
  (%m4 (%v4 (m2ref-m m x x) (m2ref-m m y x) 0.0 0.0)
       (%v4 (m2ref-m m x y) (m2ref-m m y y) 0.0 0.0)
       (%v4 0.0 0.0 ww 0.0)
       (%v4 0.0 0.0 0.0 ww)))
  

;;Printers
(defun print-m2 (m stream depth)
  (declare (ignore depth))
  (format stream "M2~%")
  (format stream "~{  ~{~8f~^ ~}~^~%~}"
          (mapcar #'list (v2-list (m2-x m))
                (v2-list (m2-y m)))))

(defun print-m3 (m stream depth)
  (declare (ignore depth))
  (format stream "M3~%")
  (format stream "~{  ~{~8f~^ ~}~^~%~}"
          (mapcar #'list (v3-list (m3-x m))
                (v3-list (m3-y m))
                (v3-list (m3-z m)))))

(defun print-m4 (m stream depth)
  (declare (ignore depth))
  (format stream "M4~%")
  (format stream "~{  ~{~8f~^ ~}~^~%~}"
          (mapcar #'list (v4-list (m4-x m))
                (v4-list (m4-y m))
                (v4-list (m4-z m))
                (v4-list (m4-w m)))))

;;Operations

;;Add
(defun %m2+ (ma mb)
  (m-combine (ma mb) :by (x y)
                :with (m2-{F} {O})
                :group gv:%v2+
                :all %m2))

(defun %m3+ (ma mb)
  (m-combine (ma mb) :by (x y z)
                :with (m3-{F} {O})
                :group gv:%v3+
                :all %m3))

(defun %m4+ (ma mb)
  (m-combine (ma mb) :by (x y z w)
                :with (m4-{F} {O})
                :group gv:%v4+
                :all %m4))

(defrecop m2+ %m2+)
(defrecop m3+ %m3+)
(defrecop m4+ %m4+)

;;Sub
(defun %m2- (ma mb)
  (m-combine (ma mb) :by (x y)
                :with (m2-{F} {O})
                :group gv:%v2-
                :all %m2))

(defun %m3- (ma mb)
  (m-combine (ma mb) :by (x y z)
                :with (m3-{F} {O})
                :group gv:%v3-
                :all %m3))

(defun %m4- (ma mb)
  (m-combine (ma mb) :by (x y z w)
                :with (m4-{F} {O})
                :group gv:%v4-
                :all %m4))

(defrecop m2- %m2-)
(defrecop m3- %m3-)
(defrecop m4- %m4-)

;;Scalar Multiplication division
(defun m2scale (m a)
  (m-combine (m) :by (x y)
             :with (m2-{F} {O})
             :each (v2scale {X} a)
             :all %m2))

(defun m3scale (m a)
  (m-combine (m) :by (x y z)
             :with (m3-{F} {O})
             :each (v3scale {X} a)
             :all %m3))

(defun m4scale (m a)
  (m-combine (m) :by (x y z w)
             :with (m4-{F} {O})
             :each (v4scale {X} a)
             :all %m4))

;;mul
(defmacro gen-m* (size)
  (let ((lst (case size
               (2 '(x y))
               (3 '(x y z))
               (4 '(x y z w)))))
    (let ((forms (loop for m2f in lst
                    collect `(%v{S} ,@(loop for m1f in lst
                                         collect `(+ ,@(loop for v in lst
                                                          collect `(* ,(f-replace-with `(v{S}-{B} (m{S}-{A} ma))
                                                                                       (list (list '{A} v)
                                                                                             (list '{B} m1f)
                                                                                             (list '{S} size)))
                                                                      ,(f-replace-with `(v{S}-{B} (m{S}-{A} mb))
                                                                                       (list (list '{A} m2f)
                                                                                             (list '{B} v)
                                                                                             (list '{S} size)))))))))))
      (f-apply (f-replace-with '%m{S} (list (list '{S} size)))
               (f-replace-with forms (list (list '{S} size)))))))
;;(gem-m* 3)
              

;; Maatrix *                      
(defun %m2* (ma mb)
  (gen-m* 2))
  
(defun %m3* (ma mb)
 (gen-m* 3))

(defun %m4* (ma mb)
  (gen-m* 4))

(defrecop m2* %m2*)
(defrecop m3* %m3*)
(defrecop m4* %m4*)


;;Determinant
(defun m2det (m)
  (- (* (m2ref-m m x x) (m2ref-m m y y))
     (* (m2ref-m m y x) (m2ref-m m x y))))

(defun m3det (m)
  (- (+ (* (m3ref-m m x x) (m3ref-m m y y) (m3ref-m m z z))
        (* (m3ref-m m x y) (m3ref-m m y z) (m3ref-m m z x))
        (* (m3ref-m m x z) (m3ref-m m y x) (m3ref-m m z y)))
     (* (m3ref-m m x z) (m3ref-m m y y) (m3ref-m m z x))
     (* (m3ref-m m x x) (m3ref-m m y z) (m3ref-m m z y))
     (* (m3ref-m m x y) (m3ref-m m y x) (m3ref-m m z z))))

(defun m4det (m)
  (let ((xx (m4ref-m m x x))
        (yx (m4ref-m m x y))
        (zx (m4ref-m m x z))
        (wx (m4ref-m m x w))
        (mx (apply #'m3
                   (append (cdr (v4-list (m4-y m)))
                           (cdr (v4-list (m4-z m)))
                           (cdr (v4-list (m4-w m))))))
        (my (apply #'m3
                   (append (cdr (v4-list (m4-x m)))
                           (cdr (v4-list (m4-z m)))
                           (cdr (v4-list (m4-w m))))))
        (mz (apply #'m3
                   (append (cdr (v4-list (m4-x m)))
                           (cdr (v4-list (m4-y m)))
                           (cdr (v4-list (m4-w m))))))
        (mw (apply #'m3
                   (append (cdr (v4-list (m4-x m)))
                           (cdr (v4-list (m4-y m)))
                           (cdr (v4-list (m4-z m)))))))
    (- (+ (* xx (m3det mx))
          (* zx (m3det mz)))
       (* yx (m3det my))
       (* wx (m3det mw)))))


;;Transpose
(defun m2t (m)
  (m-combine (x y) :by (x y)
             :with (m2ref-m m {f} {o})
             :group %v2
             :all %m2))

(defun m3t (m)
  (m-combine (x y z) :by (x y z)
             :with (m3ref-m m {F} {O})
             :group %v3
             :all %m3))

(defun m4t (m)
  (m-combine (x y z w) :by (x y z w)
             :with (m4ref-m m {F} {O})
             :group %v4
             :all %m4))


;;Minor
(defmacro m2min-m (ma r c)
  (alexandria:once-only ((m ma))
    (flet ((invi (i)
             (case i
               ((1 x) 'y)
                 ((2 y) 'x))))
      (let ((invr (invi r))
            (invc (invi c)))
        `(m2ref-m ,m ,invr ,invc)))))

(defun m2min (m r c)
   (flet ((invi (i)
             (case i
               ((0 x) 0)
                 ((1 y) 1))))
      (let ((invr (invi r))
            (invc (invi c)))
        (m2ref m invr invc))))

(defmacro m3min-m (ma r c)
  (alexandria:once-only ((m ma))
    `(m2det (m2 ,@(loop for lc in '(x y z)
                     append (loop for lr in '(x y z)
                               unless (or (eql lc (mat-index c)) (eql lr (mat-index r)))
                               collect `(m3ref-m ,m ,lr ,lc)))))))

(defun m3min (m r c)
  (m2det (apply #'m2 (loop for lc from 0 to 2
                append (loop for lr from 0 to 2
                          unless (or (= lc (mat-index-n c)) (= lr (mat-index-n r)))
                          collect (m3ref m lr lc))))))

(defmacro m4min-m (ma r c)
  (alexandria:once-only ((m ma))
    `(m3det (m3 ,@(loop for lc in '(x y z w)
                     append (loop for lr in '(x y z w)
                               unless (or (eql lc (mat-index c)) (eql lr (mat-index r)))
                               collect `(m4ref-m ,m ,lr ,lc)))))))

(defun m4min (m r c)
  (m3det (apply #'m3 (loop for lc from 0 to 3
                append (loop for lr from 0 to 3
                          unless (or (= lc (mat-index-n c)) (= lr (mat-index-n r)))
                          collect (m4ref m lr lc))))))
;;(m2min-m mm 1 2)
;;(m3min-m mm 1 2)
;;(m4min-m mm 1 2)

;;Inverse
(defun m2inv (m)
  (let ((det (m2det m)))
    (if (>= (abs det) 0.00001)
        (m2scale (m2 (m2ref-m m y y)     (- (m2ref-m m y x))
                     (- (m2ref-m m x y)) (m2ref-m m x x))
                 (/ 1 (m2det m)))
        nil)))

(defmacro m2inv-m (m)
  `(m2inv ,m))

(defmacro m3inv-m (ma)
  (alexandria:once-only ((m ma))
    (let ((mdet `(m3det ,m))
          (mn `(m3 ,@(loop for lc from 0 to 2
                     append (loop for lr from 0 to 2
                               collect (if (evenp (+ lr lc) )
                                           `(m3min-m ,m ,lr ,lc)
                                           `(- (m3min-m ,m ,lr ,lc))))))))
      `(m3scale (m3t ,mn) (/ 1 ,mdet)))))

(defun m3inv (m)
  (let ((mdet (m3det m))
          (mn (apply #'m3 (loop for lc from 0 to 2
                             append (loop for lr from 0 to 2
                                       collect (if (evenp (+ lr lc) )
                                                   (m3min m lr lc)
                                                   (- (m3min m lr lc))))))))
      (m3scale (m3t mn) (/ 1 mdet))))


(defmacro m4inv-m (ma)
  (alexandria:once-only ((m ma))
    (let ((mdet `(m4det ,m))
          (mn `(m4 ,@(loop for lc from 0 to 3
                     append (loop for lr from 0 to 3
                               collect (if (evenp (+ lr lc) )
                                           `(m4min-m ,m ,lr ,lc)
                                           `(- (m4min-m ,m ,lr ,lc))))))))
      `(m4scale (m4t ,mn) (/ 1 ,mdet)))))

(defun m4inv (m)
  (let ((mdet (m4det m))
          (mn (apply #'m4 (loop for lc from 0 to 3
                             append (loop for lr from 0 to 3
                                       collect (if (evenp (+ lr lc) )
                                                   (m4min m lr lc)
                                                   (- (m4min m lr lc))))))))
      (m4scale (m4t mn) (/ 1 mdet))))


;;3D func
(defun perspective-m4 (left right top bottom near far)
  (let ((l left) (r right) (to top) (b bottom) (n near) (f far))
    (%m4 (%v4 (/ (* 2 n) (- r l)) 0.0 0.0 0.0)
         (%v4 0.0 (/ (* 2 n) (- to b)) 0.0 0.0)
         (%v4 (/ (+ r l) (- r l))
              (/ (+ to b) (- to b))
              (-(/ (+ f n) (- f n)))
              -1.0)
         (%v4 0.0 0.0 (- (/ (* 2 f n) (- f n))) 0.0))))

(defun fov-m4 (fovr aspect near far)
  (let* ((fov (coerce (deg-to-rad fovr) 'single-float))
         (top (* (tan (/ fov 2)) near))
         (bot (- top))
         (right (* top aspect))
         (left (- right)))
    (perspective-m4 left right top bot near far)))

(defun orthographic-m4 (left right top bottom near far)
  (let ((l left) (r right) (to top) (b bottom) (n near) (f far))
    (%m4 (%v4 (/ 2 (- r l)) 0.0 0.0 0.0)
         (%v4 0.0 (/ 2 (- to b)) 0.0 0.0)
         (%v4 0.0 0.0 (/ -2 (- f n)) 0.0)
         (%v4 (- (/ (+ r l) (- r l)))
              (- (/ (+ to b) (- to b)))
              (- (/ (+ f n) (- f n)))
              1.0))))


(defun translation-m4 (&optional (x 1.0) (y 1.0) (z 1.0))
  (%m4 (%v4 1.0 0.0 0.0 0.0)
       (%v4 0.0 1.0 0.0 0.0)
       (%v4 0.0 0.0 1.0 0.0)
       (%v4 x   y   z   1.0)))

(defun scaling-m4 (x y z)
  (%m4 (%v4 x   0.0 0.0 0.0)
       (%v4 0.0 y   0.0 0.0)
       (%v4 0.0 0.0 z   0.0)
       (%v4 0.0 0.0 0.0 1.0)))

(defun rotation-x-m4 (o)
  (%m4 (%v4 1.0 0.0     0.0 0.0)
       (%v4 0.0 (cos o) (sin o) 0.0)
       (%v4 0.0 (-(sin o)) (cos o) 0.0)
       (%v4 0.0 0.0 0.0 1.0)))

(defun rotation-y-m4 (o)
  (%m4 (%v4 (cos o) 0.0  (-(sin o)) 0.0)
       (%v4 0.0 1.0 0.0 0.0)
       (%v4 (sin o) 0.0 (cos o) 0.0)
       (%v4 0.0 0.0 0.0 1.0)))

(defun rotation-z-m4 (o)
  (%m4 (%v4 (cos o) (sin o)    0.0 0.0)
       (%v4 (-(sin o)) (cos o) 0.0 0.0)
       (%v4 0.0 0.0            1.0 0.0)
       (%v4 0.0 0.0            0.0 1.0)))







 
