(defpackage #:cl-gamelib-matrix
  (:nicknames #:cl-gal-m #:gal-m #:gm)
  (:use #:cl #:cl-user #:cl-gamelib-vector #:sb-alien #:gh)
  (:export :m2 :m3 :m4
           :%m2 :%m3 :%m4

           :m2-to-m3 :m3-to-m4 :m2-to-m4
           
           :m2ref :m3ref :m4ref

           :m2ref-m :m3ref-m :m4ref-m
           :m2-list :m3-list :m4-list
           :m2arr :m3arr :m4arr

           :%m2+ :%m3+ :%m4+
           :m2+ :m3+ :m4+

           :%m2- :%m3- :%m4-
           :m2- :m3- :m4-

           :m2scal :m3scal :m4scal

           :%m2* :%m3* :%m4*
           :m2* :m3* :m4*

           :m2det :m3det :m4det
           :m2t :m3t :m4t

           :m2min-m :m3min-m :m4min-m
           :m2min :m3min :m4min

           :m2inv-m :m3inv-m :m4inv-m
           :m2inv :m3inv :m4inv

           :perspective-m4
           :fov-m4
           :orthographic-m4
           
           :translation-m4
           :scaling-m4
           :rotation-x-m4 :rotation-y-m4 :rotation-z-m4))
