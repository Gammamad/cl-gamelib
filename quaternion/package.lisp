(defpackage #:cl-gamelib-quaternion
  (:nicknames #:cl-gal-q #:gal-q #:gq)
  (:use #:cl #:gv #:gh #:gm)
  (:export :quat
           :quat-x :quat-y :quat-z :quat-w
           :q-mat
           :q-rotation
           :q*))

