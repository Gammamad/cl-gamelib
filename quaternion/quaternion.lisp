(in-package #:cl-gamelib-quaternion)

(defun quat (&optional (x 0.0) (y 0.0) (z 0.0) (w 0.0))
  (gv:v4 x y z w))

(defun q-x (q)
  (v4-x q))

(defun set-qx (q v)
  (setf (gv:v4-x q) v))

(defsetf q-x set-qx) 

(defun q-y (q)
  (v4-y q))

(defun set-qy (q v)
  (setf (gv:v4-y q) v))

(defsetf q-y set-qy) 

(defun q-z (q)
  (v4-z q))

(defun set-qz (q v)
  (setf (gv:v4-z q) v))

(defsetf q-z set-qz) 

(defun q-w (q)
  (v4-w q))

(defun set-qw (q v)
  (setf (gv:v4-w q) v))

(defsetf q-w set-qw) 

(defun q* (a b)
  (gv:%v4 (- (+ (* (q-w a) (q-x b))
                (* (q-x a) (q-w b))
                (* (q-y a) (q-z b)))
             (* (q-z a) (q-y b)))
          (- (+ (* (q-w a) (q-y b))
                (* (q-y a) (q-w b))
                (* (q-z a) (q-x b)))
             (* (q-x a) (q-z b)))
          (- (+ (* (q-w a) (q-z b))
                (* (q-z a) (q-w b))
                (* (q-x a) (q-y b)))
             (* (q-y a) (q-x b)))
          (- (* (q-w a) (q-w b)) (* (q-x a) (q-x b))
             (* (q-y a) (q-y b)) (* (q-z a) (q-z b)))))

(defun q-rotation (axis angle)
  (let* ((a/2 (/ (gh:deg-to-rad angle) 2))
         (sin/2 (sin a/2))
         (cos/2 (cos a/2)))
    (gv:v4 (* (q-x axis) sin/2)
            (* (q-y axis) sin/2)
            (* (q-z axis) sin/2)
            cos/2)))

(defun q-mat (q)
  (gm:%m4
   (gv:%v4 (- 1.0 (* 2.0 (+ (* (q-y q) (q-y q)) (* (q-z q) (q-z q)))))
           (* 2.0 (+ (*(q-x q) (q-y q)) (* (q-w q) (q-z q))))
           (* 2.0 (- (*(q-x q) (q-z q)) (* (q-w q) (q-y q))))
           0.0)
   (gv:%v4 (* 2.0 (- (* (q-x q) (q-y q)) (* (q-w q) (q-z q))))
           (- 1.0 (* 2.0 (+ (* (q-x q) (q-x q)) (* (q-z q) (q-z q)))))
           (* 2.0 (+ (* (q-y q) (q-z q)) (* (q-w q) (q-x q))))
           0.0)
   (gv:%v4 (* 2.0 (+ (* (q-x q) (q-z q)) (* (q-w q) (q-y q))))
           (* 2.0 (- (* (q-y q) (q-z q)) (* (q-w q) (q-x q))))
           (- 1.0 (* 2.0 (+ (* (q-x q) (q-x q)) (* (q-y q) (q-y q)))))
           0.0)
   (gv:%v4 0.0 0.0 0.0 1.0)))
             

