(defpackage cl-gamelib-vector
  (:nicknames #:cl-gal-v #:gal-v #:gv)
  (:use #:cl #:cl-gamelib-helpers #:sb-alien)
  (:export :v4unit :v3unit :v2unit
           :v4mag :v3mag :v2mag
           :v4mags :v3mags :v2mags
           :v4/ :v3/ :v2/
           :v4dot :v3dot :v2dot
           
           :%v4/ :%v3/ :%v2/
           :%v4* :%v3* :%v2*
           
           :v4/ :v3/ :v2/
           :v4* :v3* :v2*
           
           :v4scale :v3scale :v2scale
           
           :v4- :v3- :v2-
           :v4+ :v3+ :v2+
           :%v4- :%v3- :%v2-
           :%v4+ :%v3+ :%v2+

           :v4-list :v3-list :v2-list
           :v2 :v3 :v4
           :%v2 :%v3 :%v4
           :v2-to-3 :v2-to-4 :v3-to-4
           :v3x

           
           :v2-x :v2-y
           :v3-x :v3-y :v3-z
           :v4-x :v4-y :v4-z :v4-w))
