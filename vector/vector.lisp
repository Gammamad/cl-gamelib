(in-package #:cl-gamelib-vector)

(eval-when (:compile-toplevel :load-toplevel :execute))

(defstruct (v2 (:conc-name v2-) (:constructor) (:constructor %v2 (x y)))
  (x 0 :type single-float)
  (y 0 :type single-float))

(defstruct
    (v3 (:conc-name v3-) (:include v2) (:constructor)
        (:constructor %v3 (x y z)))
  (z 0 :type single-float))

(defstruct
    (v4 (:conc-name v4-) (:include v3) (:constructor)
        (:constructor %v4 (x y z w)))
  (w 0.0 :type single-float))

(defun v2 (&optional (x 0.0) (y 0.0))
  (%v2 (coerce x 'single-float) (coerce y 'single-float)))

(defun v3 (&optional (x 0.0) (y 0.0) (z 0.0))
  (%v3 (coerce x 'single-float) (coerce y 'single-float)
       (coerce z 'single-float)))

(defun v4 (&optional (x 0.0) (y 0.0) (z 0.0) (w 0.0))
  (%v4 (coerce x 'single-float) (coerce y 'single-float)
       (coerce z 'single-float) (coerce w 'single-float)))

(defun v2-to-3 (v &optional (z 1.0))
  (%v3 (v2-x v) (v2-y v) (coerce z 'single-float)))

(defun v2-to-4 (v &optional (z 1.0) (w 1.0))
  (%v4 (v2-x v) (v2-y v) (coerce z 'single-float) (coerce w 'single-float)))

(defun v3-to-4 (v &optional (w 1.0))
  (%v4 (v3-x v) (v3-y v) (v3-z v) (coerce w 'single-float)))

(defun v2-list (v2) (m-combine (v2) :by (x y) :with (v2-{f} {o}) :all list))

(defun v3-list (v3)
  (m-combine (v3) :by (x y z) :with (v3-{f} {o}) :all list))

(defun v4-list (v4)
  (m-combine (v4) :by (x y z w) :with (v4-{f} {o}) :all list))

(defun v2arr (v)
  (let ((res (make-array 2 :type 'single-float)))
    (setf (aref res 0) (v2-x)) 
    (setf (aref res 1) (v2-y))
    res))

(defun v3arr (v)
  (let ((res (make-array 3 :type 'single-float)))
    (setf (aref res 0) (v2-x)) 
    (setf (aref res 1) (v2-y))
    (setf (aref res 2) (v2-z))
    res))

(defun v4arr (v)
  (let ((res (make-array 4 :type 'single-float)))
    (setf (aref res 0) (v2-x)) 
    (setf (aref res 1) (v2-y))
    (setf (aref res 2) (v2-z))
    (setf (aref res 3) (v2-w))
    res))


(declaim (inline %v2+))
(defun %v2+ (va vb)
  (m-combine (va vb) :by (x y) :with (v2-{f} {o}) :group + :all %v2))

(defun %v3+ (va vb)
  (m-combine (va vb) :by (x y z) :with (v3-{f} {o}) :group + :all %v3))

(defun %v4+ (va vb)
  (m-combine (va vb) :by (x y z w) :with (v4-{f} {o}) :group + :all %v4))

(defrecop v2+ %v2+)
(defrecop v3+ %v3+)
(defrecop v4+ %v4+)

(defun %v2- (va vb)
  (m-combine (va vb) :by (x y) :with (v2-{f} {o}) :group - :all %v2))

(defun %v3- (va vb)
  (m-combine (va vb) :by (x y z) :with (v3-{f} {o}) :group - :all %v3))

(defun %v4- (va vb)
  (m-combine (va vb) :by (x y z w) :with (v4-{f} {o}) :group - :all %v4))

(defrecop v2- %v2-)
(defrecop v3- %v3-)
(defrecop v4- %v4-)

(defun v2scale (v s)
  (m-combine (v) :by (x y) :with (v2-{f} {o}) :all %v2 :each (* {x} s)))

(defun v3scale (v s)
  (m-combine (v) :by (x y z) :with (v3-{f} {o}) :all %v3 :each (* {x} s)))

(defun v4scale (v s)
  (m-combine (v) :by (x y z w) :with (v4-{f} {o}) :all %v4 :each (* {x} s)))

(defun %v2* (va vb)
  (m-combine (va vb) :by (x y) :with (v2-{f} {o}) :group * :all %v2))

(defun %v3* (va vb)
  (m-combine (va vb) :by (x y z) :with (v3-{f} {o}) :group * :all %v3))

(defun %v4* (va vb)
  (m-combine (va vb) :by (x y z w) :with (v4-{f} {o}) :group * :all %v4))

(defrecop v2* %v2*)
(defrecop v3* %v3*)
(defrecop v4* %v4*)

(defun %v2/ (va vb)
  (m-combine (va vb) :by (x y) :with (v2-{f} {o}) :group / :all %v2))

(defun %v3/ (va vb)
  (m-combine (va vb) :by (x y z) :with (v3-{f} {o}) :group / :all %v3))

(defun %v4/ (va vb)
  (m-combine (va vb) :by (x y z w) :with (v4-{f} {o}) :group / :all %v4))

(defrecop v2/ %v2/)
(defrecop v3/ %v3/)
(defrecop v4/ %v4/)

(defun v3x (va vb)
  (%v3 (- (* (v3-y va) (v3-z vb)) (* (v3-z va) (v3-y vb)))
       (- (* (v3-z va) (v3-x vb)) (* (v3-x va) (v3-z vb)))
       (- (* (v3-x va) (v3-y vb)) (* (v3-y va) (v3-x vb)))))

(defun %v2dot (va vb)
  (m-combine (va vb) :by (x y) :with (v2-{f} {o}) :group * :all +))

(defun %v3dot (va vb)
  (m-combine (va vb) :by (x y z) :with (v3-{f} {o}) :group * :all +))

(defun %v4dot (va vb)
  (m-combine (va vb) :by (x y z w) :with (v4-{f} {o}) :group * :all +))

(defrecop v2* %v2*)
(defrecop v3* %v3*)
(defrecop v4* %v4*)

(defun v2mags (v)
  (m-combine (v) :by (x y) :with (v2-{f} {o}) :each (* {x} {x}) :all +))

(defun v3mags (v)
  (m-combine (v) :by (x y z) :with (v3-{f} {o}) :each (* {x} {x}) :all +))

(defun v4mags (v)
  (m-combine (v) :by (x y z w) :with (v4-{f} {o}) :each (* {x} {x}) :all +))

(defun v2mag (v) (sqrt (v2mags v)))

(defun v3mag (v) (sqrt (v3mags v)))

(defun v4mag (v) (sqrt (v4mags v)))

(defun v2unit (v)
  "make unit vtor of same direction"
  (let ((magnitude (v2mag v)))
    (m-combine (v) :by (x y) :with (v2-{f} {o}) :each (/ {x} magnitude) :all
               %v2)))

(defun v2unitp (v)
    (if (gh:float= (v2mags v) 1.0)
      t
      nil))

(defun v3unit (v)
  "make unit vtor of same direction"
  (let ((magnitude (v3mag v)))
    (m-combine (v) :by (x y z) :with (v3-{f} {o}) :each (/ {x} magnitude)
               :all %v3)))

(defun v3unitp (v)
  (if (gh:float= (v3mags v) 1.0)
      t
      nil))

(defun v4unit (v)
  "make unit vtor of same direction"
  (let ((magnitude (v4mag v)))
    (m-combine (v) :by (x y z w) :with (v4-{f} {o}) :each (/ {x} magnitude)
               :all %v4)))

(defun v4unitp (v)
  (if (gh:float= (v4mags v) 1.0)
      t
      nil))

(defun v2= (va vb)
  (m-combine (va vb) :by (x y) :with (v2-{f} {o}) :group =
             :all and))

(defun v3= (va vb)
  (m-combine (va vb) :by (x y z) :with (v3-{f} {o}) :group =
             :all and))

(defun v4= (va vb)
  (m-combine (va vb) :by (x y z w) :with (v4-{f} {o}) :group =
               :all and))
